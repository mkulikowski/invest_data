from lxml import etree

class Config:
    def __init__(self, schema_file_name, config_file_name):
        schema_file = open(schema_file_name, 'r')

        schema_root = etree.XML(schema_file.read())
        schema = etree.XMLSchema(schema_root)

        parser = etree.XMLParser(schema=schema)
        self._tree = etree.parse(config_file_name, parser)

    def get(self, param):
        return ConfigNode(self._tree.find(param))

class ConfigNode:
    def __init__(self, node):
        self._node = node

    def value(self):
        return self._node.text

    def get(self, param):
        return ConfigNode(self._node.find(param))

