import unittest
from config import Config

class TestStringMethods(unittest.TestCase):

    def test_top_level_element(self):
        config = Config("shiporder.xsd", "shiporder.xml")
        self.assertEqual(config.get("orderperson").value(), "John Smith")

    def test_inner_element(self):
        config = Config("shiporder.xsd", "shiporder.xml")
        self.assertEqual(config.get("item").get("title").value(), "Empire Burlesque")


if __name__ == '__main__':
    unittest.main()