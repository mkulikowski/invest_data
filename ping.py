import socket
import threading
import time


class RemoteServiceWatchdog:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self._stopped = False
        self._do_work()

        self.thread = threading.Thread(target=self._worker)
        self.thread.setDaemon(True)
        self.thread.start()

    def _worker(self):
        while not self._stopped:
            self._do_work()
            time.sleep(30)

    def _do_work(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.SOCK_STREAM)
        except socket.error as e:
            raise EnvironmentError(e)
        try:
            s.connect((socket.gethostbyname(self.host), self.port))
            s.close()
            self._alive = True
        except socket.error:
            self._alive = False

    def is_alive(self):
        return self._alive

    def stop(self):
        self._stopped = True
